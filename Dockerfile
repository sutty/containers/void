FROM alpine:3.10 AS build
MAINTAINER "f <f@sutty.nl>"

RUN apk add --no-cache ca-certificates wget
RUN wget https://a-hel-fi.m.voidlinux.org/live/current/void-x86_64-ROOTFS-20181111.tar.xz

WORKDIR /void

RUN tar xf /void-x86_64-ROOTFS-20181111.tar.xz
RUN cp /etc/resolv.conf etc/
RUN chroot . xbps-install --sync --update --yes

COPY ./remove.txt /tmp
COPY ./remove_dirs.txt /tmp
COPY ./symlinks.sh /usr/local/bin/symlinks

RUN cat /tmp/remove.txt | xargs chroot . xbps-remove --yes
RUN chroot . xbps-install --yes busybox
RUN chroot . xbps-remove --recursive --yes coreutils
RUN cat /tmp/remove_dirs.txt | xargs rm -rf
RUN rm var/cache/xbps/*
RUN symlinks /void

FROM scratch
COPY --from=build /void /
