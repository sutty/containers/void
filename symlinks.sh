#!/bin/sh

chroot $1 busybox --help \
  | grep , \
  | tr -d [:space:] \
  | tr , "\n" \
  | while read cmd; do ln -s busybox $1/usr/bin/$cmd; done
